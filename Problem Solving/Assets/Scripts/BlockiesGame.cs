﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockiesGame : MonoBehaviour
{
    private SpriteRenderer _sprite;

    public float resetDuration = 3f;
    [SerializeField]
    private bool _resetAfterTrigger = false;
    public bool canTrigger = false;
    public float moveDuration = 10f;
    private float _moveDuration;
    private GameManager _gameManager;

    public float difficultyModifier;

    private float _resetDuration = 0f;
    // Start is called before the first frame update
    void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
        _gameManager = GameManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        if(_resetAfterTrigger && _resetDuration > 0f)
        {
            _resetDuration -= Time.deltaTime;
        }
        else if(_resetAfterTrigger && !canTrigger && _resetDuration <= 0f)
        {
            canTrigger = true;
            StartCoroutine(BlockiesManagerGame.Instance.GenerateBlockies(transform));
            SetBlockiesVisible();
        }

        /*if(_moveDuration > 0)
        {
            _moveDuration -= Time.deltaTime;
        }
        else if(_moveDuration <= 0)
        {
            StartCoroutine(BlockiesManagerGame.Instance.GenerateBlockies(transform));
            _moveDuration = moveDuration;
        }
        */
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && canTrigger)
        {
            GameManager.Instance.ResetComboTimer();
            ScoreManager.Instance.AddScore();
            canTrigger = false;
            _gameManager.playerHealth += GameManager.Instance.selectedDifficulty.playerRegen;
            resetDuration += difficultyModifier;
            SetBlockiesInvisible();
            if (_resetAfterTrigger)
            {
                _resetDuration = resetDuration;
            }
        }
    }

    public void SetBlockiesInvisible(System.Action OnCompleted = null)
    {
        _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 0f);
    }

    public void SetBlockiesVisible()
    {
        _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 1f);
    }
}
