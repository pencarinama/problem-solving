﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockiesManagerGame : MonoBehaviour
{
    private static BlockiesManagerGame _instance;
    public static BlockiesManagerGame Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.FindObjectOfType<BlockiesManagerGame>();
            }
            return _instance;
        }
    }

    public Vector2 minimumInstancePosition;
    public Vector2 maximumInstancePosition;
    public int minInstanceAmount;
    public int maxInstanceAmount;
    public GameObject blockiesPrefab;
    public GameObject player;

    private List<GameObject> allBlockies = new List<GameObject>();
    int _id = 0;
    // Start is called before the first frame update
    void Start()
    {
    }

    public void StartGame()
    {
        StartCoroutine(GenerateBlockies());
    }

    public Vector2 GetRandomPosition()
    {

        float xPosition = Random.Range(minimumInstancePosition.x, maximumInstancePosition.x);
        float yPosition = Random.Range(minimumInstancePosition.y, maximumInstancePosition.y);

        return new Vector2(xPosition, yPosition);
    }
    public void ResetPosition(Transform instance)
    {

        float xPosition = Random.Range(minimumInstancePosition.x, maximumInstancePosition.x);
        float yPosition = Random.Range(minimumInstancePosition.y, maximumInstancePosition.y);
        instance.position = new Vector2(xPosition, yPosition);
    }

    public bool CollidedWithOtherObject(Vector2 position, float size)
    {
        Collider2D[] collision = Physics2D.OverlapBoxAll(position, new Vector2(size, size), 0f);
        if(collision == null || collision.Length <= 0)
        {
            return false;
        }
        return true;
    }

    public IEnumerator GenerateBlockies()
    {
        minInstanceAmount = GameManager.Instance.selectedDifficulty.minimumSpawn;
        maxInstanceAmount = GameManager.Instance.selectedDifficulty.maximumSpawn;
        int rnd = Random.Range(minInstanceAmount, maxInstanceAmount);

        for (int q = 0; q < rnd; q++)
        {
            SpriteRenderer _sprite;
            Vector2 _spritePosition = GetRandomPosition();

            GameObject instance = Instantiate(blockiesPrefab, transform);
            _sprite = instance.GetComponent<SpriteRenderer>();
            _sprite.name += q;
            //_id = q;
            instance.transform.position = _spritePosition;
            //check if sprite overlap with other object
            while (CollidedWithOtherObject(_spritePosition, _sprite.size.x * 2f))
            {
                _spritePosition = GetRandomPosition();
                instance.transform.position = _spritePosition;
            }
            BlockiesGame blockies = instance.GetComponent<BlockiesGame>();
            blockies.canTrigger = true;

            allBlockies.Add(instance);
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator GenerateBlockies(Transform transform)
    {
        SpriteRenderer _sprite = transform.GetComponent<SpriteRenderer>();
        Vector2 _spritePosition = GetRandomPosition();
        transform.position = _spritePosition;
        _sprite = transform.GetComponent<SpriteRenderer>();
        //check if sprite overlap with other object
        while (CollidedWithOtherObject(_spritePosition, _sprite.size.x * 2f))
        {
            _spritePosition = GetRandomPosition();
            transform.position = _spritePosition;
        }
        BlockiesGame blockies = transform.GetComponent<BlockiesGame>();
        blockies.canTrigger = true;
        yield return new WaitForEndOfFrame();
    }

    public void ClearBlockies()
    {
        foreach (GameObject blockies in allBlockies)
        {
            Destroy(blockies);
        }
        allBlockies.Clear();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
