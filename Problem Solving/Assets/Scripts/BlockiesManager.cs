﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockiesManager : MonoBehaviour
{
    private static BlockiesManager _instance;
    public static BlockiesManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = GameObject.FindObjectOfType<BlockiesManager>();
            }
            return _instance;
        }
    }

    public Vector2 minimumInstancePosition;
    public Vector2 maximumInstancePosition;
    public int minInstanceAmount;
    public int maxInstanceAmount;
    public GameObject blockiesPrefab;
    public GameObject player;
    private static readonly Vector2[] _directions = new Vector2[] { Vector2.up, Vector2.down, Vector2.left, Vector2.right };
    int _id = 0;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GenerateBlockies());
    }

    public Vector2 GetRandomPosition()
    {

        float xPosition = Random.Range(minimumInstancePosition.x, maximumInstancePosition.x);
        float yPosition = Random.Range(minimumInstancePosition.y, maximumInstancePosition.y);

        return new Vector2(xPosition, yPosition);
    }
    public void ResetPosition(Transform instance)
    {

        float xPosition = Random.Range(minimumInstancePosition.x, maximumInstancePosition.x);
        float yPosition = Random.Range(minimumInstancePosition.y, maximumInstancePosition.y);
        instance.position = new Vector2(xPosition, yPosition);
    }

    public bool CollidedWithOtherObject(Vector2 position, float size)
    {
        Collider2D[] collision = Physics2D.OverlapBoxAll(position, new Vector2(size, size), 0f);
        if(collision == null || collision.Length <= 0)
        {
            return false;
        }
        return true;
    }

    public IEnumerator GenerateBlockies()
    {
        int rnd = Random.Range(minInstanceAmount, maxInstanceAmount);

        for (int q = 0; q < rnd; q++)
        {
            SpriteRenderer _sprite;
            Vector2 _spritePosition = GetRandomPosition();

            GameObject instance = Instantiate(blockiesPrefab, transform);
            _sprite = instance.GetComponent<SpriteRenderer>();
            _sprite.name += q;
            //_id = q;
            instance.transform.position = _spritePosition;
            //check if sprite overlap with other object
            while (CollidedWithOtherObject(_spritePosition, _sprite.size.x * 2f))
            {
                _spritePosition = GetRandomPosition();
                instance.transform.position = _spritePosition;
            }
            Blockies blockies = instance.GetComponent<Blockies>();
            if(blockies!= null)
            {
                blockies.canTrigger = true;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator GenerateBlockies(Transform transform)
    {
        SpriteRenderer _sprite = transform.GetComponent<SpriteRenderer>();
        Vector2 _spritePosition = GetRandomPosition();

        _sprite = transform.GetComponent<SpriteRenderer>();
        //check if sprite overlap with other object
        while (CollidedWithOtherObject(_spritePosition, _sprite.size.x * 2f))
        {
            _spritePosition = GetRandomPosition();
            transform.position = _spritePosition;
        }
        Blockies blockies = transform.GetComponent<Blockies>();
        if (blockies != null)
        {
            blockies.canTrigger = true;
        }
        yield return new WaitForEndOfFrame();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
