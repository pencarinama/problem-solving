﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    public Text scoreText;
    private static ScoreManager _instance;
    public bool isGame = false;
    public static ScoreManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<ScoreManager>();
            }

            return _instance;
        }
    }

    private int currentScore = 0;

    public int CurrentScore
    {
        get
        {
            return currentScore;
        }
    }

    public void AddScore()
    {
        if(isGame)
        {
            int addScore = 1;
            if(GameManager.Instance.playerCombo > 0)
            {
                addScore *= GameManager.Instance.playerCombo;
            }
            currentScore += addScore;
        }
        else
        {
            currentScore += 1;
        }
        UpdateScoreText();
    }

    public void ResetScore()
    {
        currentScore = 0;
        UpdateScoreText();
    }

    private void UpdateScoreText()
    {
        if(isGame)
        {
            scoreText.text = currentScore.ToString();
        }
        else
        {
            scoreText.text = string.Format("Score : {0}", currentScore.ToString());
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        UpdateScoreText();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
