﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticMove : MonoBehaviour
{

    private Rigidbody2D _rigidBody;
    public float initialForce = 5f;
    // Start is called before the first frame update
    void Start()
    {
        _rigidBody = gameObject.GetComponent<Rigidbody2D>();

        _rigidBody.AddForce(Vector2.one * initialForce);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
