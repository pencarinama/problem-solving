﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour
{

    public float moveSpeed = 15f;

    private float _verticalDirection = 0f;
    private float _horizontalDirection = 0f;
    private Rigidbody2D _rigidBody;

    private Vector2 _mousePosition = Vector2.zero;
    private Vector2 _moveVelocity = Vector2.zero;
    private float _distance = 0f;
    // Start is called before the first frame update
    void Start()
    {

        _rigidBody = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonUp(0))
        {
            _mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            _distance = Vector2.Distance(transform.position, _mousePosition);
        }

        if(Mathf.Abs(_distance) > .1f)
        {
            transform.position = Vector2.MoveTowards(transform.position, _mousePosition, Time.deltaTime * moveSpeed);
            _distance = Vector2.Distance(transform.position, _mousePosition);
        }
        else
        {
            _distance = 0f;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Wall")
        {
            _distance = 0f;
        }
    }
}
