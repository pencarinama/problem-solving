﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blockies : MonoBehaviour
{
    private SpriteRenderer _sprite;

    public float resetDuration = 3f;
    [SerializeField]
    private bool _resetAfterTrigger = false;
    public bool canTrigger = false;

    private float _resetDuration = 0f;
    // Start is called before the first frame update
    void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(_resetAfterTrigger && _resetDuration > 0f)
        {
            _resetDuration -= Time.deltaTime;
        }
        else if(_resetAfterTrigger && !canTrigger && _resetDuration <= 0f)
        {
            canTrigger = true;
            StartCoroutine(BlockiesManager.Instance.GenerateBlockies(transform));
            SetBlockiesVisible();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" && canTrigger)
        {
            ScoreManager.Instance.AddScore();
            canTrigger = false;

            SetBlockiesInvisible();
            if (_resetAfterTrigger)
            {
                _resetDuration = resetDuration;
            }
        }
    }

    public void SetBlockiesInvisible(System.Action OnCompleted = null)
    {
        _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 0f);
    }

    public void SetBlockiesVisible()
    {
        _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 1f);
    }
}
