﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsController : MonoBehaviour
{


    private static SoundsController _instance;
    public AudioClip beepSound;
    public AudioClip beepEnd;
    public AudioClip comboUp;
    public AudioClip endGame;
    private AudioSource _audioSource;
    public static SoundsController Instance
    {
        get
        {
            if(_instance== null)
            {
                _instance = FindObjectOfType<SoundsController>();

            }
            return _instance;
        }
    }

    public void PlayThis(string clipName)
    {
        if (clipName == "combo")
        {
            _audioSource.PlayOneShot(comboUp);
        }
        if(clipName == "beep_one")
        {
            _audioSource.PlayOneShot(beepSound);
        }
        if (clipName == "beep_end")
        {
            _audioSource.PlayOneShot(beepEnd);
        }
        if(clipName == "end")
        {
            _audioSource.PlayOneShot(endGame);
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
