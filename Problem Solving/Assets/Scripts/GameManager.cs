﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{

    public GameObject player;
    public BlockiesManagerGame blockiesManager;

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }

    public List<GameDifficulty> gameDifficulties = new List<GameDifficulty>();

    public GameDifficulty selectedDifficulty;

    private bool gameIsStarted;
    private bool countdownIsStarted;

    public float playerHealth;
    private float _playerMaxHealth;
    public int playerCombo;
    private int topCombo = 0;
    public float comboTimer = 3f;
    private float _comboTimer;
    private float _fontSize = 0f;
    private float _fontMultiplier = 20f;

    public Button startGameButton;
    public Button quitGameButton;
    public GameObject mainMenuPanel;
    public Text countDownText;
    public GameObject scorePanel;
    public GameObject comboPanel;
    public Text comboText;

    public GameObject difficultySelectPanel;
    public GameObject difficultyButtonsPanel;
    public GameObject difficultyButtonPrefab;
    public GameObject healthPanel;
    public Image healthBar;
    private Vector2 _healthBarWidth;

    public GameObject GameOverPanel;
    public Text finalScoreText;
    public Text topComboText;
    public Button retryButton;
    public Button changeDifficultyButton;
    public Button mainMenuButton;

    public void ResetComboTimer()
    {
        comboPanel.GetComponent<Animator>().ResetTrigger("ComboUp");
        _comboTimer = comboTimer;
        playerCombo += 1;
        comboText.text = "x" + playerCombo.ToString();
        comboPanel.GetComponent<Animator>().SetTrigger("ComboUp");
        if (playerCombo > 1)
        {
            comboPanel.SetActive(true);
            SoundsController.Instance.PlayThis("combo");
        }

        if(playerCombo > topCombo)
        {
            topCombo = playerCombo;
        }
    }
    public void SelectDifficulty()
    {
        difficultySelectPanel.SetActive(true);
        mainMenuPanel.SetActive(false);
    }

    public void StartCountdown()
    {
        countDownText.gameObject.SetActive(true);

        countdownIsStarted = true;
        scorePanel.SetActive(true);
        healthPanel.SetActive(true);
        BlockiesManagerGame.Instance.StartGame();
        StartCoroutine(Countdown(() =>
        {
            StartGame();
        }));
    }

    public void StartGame()
    {
        player.GetComponent<ManualMovement>().enabled = true;
        _comboTimer = comboTimer;
        countDownText.gameObject.SetActive(false);
        gameIsStarted = true;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void PauseGame(bool pausing = true)
    {
        if(pausing)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }
    
    public void GameOver()
    {

        SoundsController.Instance.PlayThis("end");
        gameIsStarted = false;
        player.GetComponent<ManualMovement>().enabled = false;
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        BlockiesManagerGame.Instance.ClearBlockies();
        GameOverPanel.SetActive(true);
        finalScoreText.text = string.Format("Final score : {0}", ScoreManager.Instance.CurrentScore);
        topComboText.text = string.Format("Top combo : {0}", topCombo);
    }

    // Start is called before the first frame update
    void Start()
    {

        _healthBarWidth = healthBar.rectTransform.sizeDelta;
        startGameButton.onClick.AddListener(() =>
        {
            SelectDifficulty();
        });
        quitGameButton.onClick.AddListener(() =>
        {
            QuitGame();
        });

        retryButton.onClick.AddListener(() =>
        {
            ResetProgress();
        });
        changeDifficultyButton.onClick.AddListener(() =>
        {
            ResetProgress(true);
        });
        mainMenuButton.onClick.AddListener(() => {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        });

        foreach (GameDifficulty difficulty in gameDifficulties)
        {
            GameObject instance = Instantiate(difficultyButtonPrefab, difficultyButtonsPanel.transform);
            instance.GetComponentInChildren<Text>().text = difficulty.difficultyName;
            instance.GetComponent<Button>().onClick.AddListener(() =>
            {

                selectedDifficulty = difficulty;

                _playerMaxHealth = selectedDifficulty.playerHealth;
                playerHealth = selectedDifficulty.playerHealth;
                difficultySelectPanel.SetActive(false);
                StartCountdown();
            });
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(gameIsStarted)
        {
            if(playerHealth > _playerMaxHealth)
            {
                playerHealth = _playerMaxHealth;
            }
            playerHealth -= Time.deltaTime;
            healthBar.rectTransform.sizeDelta = new Vector2(playerHealth / _playerMaxHealth * _healthBarWidth.x, _healthBarWidth.y); 
        }
        if (playerHealth <= 0 && gameIsStarted)
        {
            GameOver();
        }

        if (countdownIsStarted)
        {
            _fontSize += Time.deltaTime * _fontMultiplier;
            countDownText.fontSize = Mathf.FloorToInt(_fontSize);
        }

        if(playerCombo >= 1)
        {
            _comboTimer -= Time.deltaTime;
        }
        if(playerCombo >= 1 && _comboTimer <= 0)
        {
            playerCombo = 0;
            comboText.text = "x1";
            comboPanel.SetActive(false);
        }
    }

    private IEnumerator Countdown(System.Action OnComplete)
    {
        int i = 3;
        while (i >= 0)
        {
            if(i == 0)
            {
                SoundsController.Instance.PlayThis("beep_end");
                countDownText.text = "GO!";
                _fontMultiplier = 30f;
            }
            else
            {
                SoundsController.Instance.PlayThis("beep_one");
                countDownText.text = i.ToString();
            }
            countDownText.fontSize = 32;
            _fontSize = 32f;
            yield return new WaitForSeconds( i > 0 ? 1.5f : 1f);
            i--;
        }

        OnComplete?.Invoke();
    }

    public void ResetProgress(bool showDifficultySettings = false)
    {
        ScoreManager.Instance.ResetScore();
        player.transform.position = Vector2.zero;
        GameOverPanel.SetActive(false);
        topCombo = 0;
        if (showDifficultySettings)
        {
            difficultySelectPanel.SetActive(true);
        }
        else
        {
            playerHealth = selectedDifficulty.playerHealth;
            healthBar.rectTransform.sizeDelta = new Vector2(playerHealth / _playerMaxHealth * _healthBarWidth.x, _healthBarWidth.y);
            StartCountdown();
        }
    }
}

[System.Serializable]
public class GameDifficulty
{
    public string difficultyName;
    public float playerSpeed;
    public float resetModifier;
    public float playerHealth;
    public float playerRegen;
    public float resetPositionDuration;
    public int minimumSpawn;
    public int maximumSpawn;
}