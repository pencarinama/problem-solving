﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualMovement : MonoBehaviour
{

    public KeyCode moveUp = KeyCode.W;
    public KeyCode moveLeft = KeyCode.A;
    public KeyCode moveDown = KeyCode.S;
    public KeyCode moveRight = KeyCode.D;

    public float moveSpeed = 15f;

    private float _verticalDirection = 0f;
    private float _horizontalDirection = 0f;
    private Rigidbody2D _rigidBody;

    private Vector2 _moveVelocity = Vector2.zero;

    // Start is called before the first frame update
    void Start()
    {

        _rigidBody = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        _moveVelocity = _rigidBody.velocity;

        if(Input.GetKey(moveUp))
        {
            _verticalDirection = 1;
        }
        else if(Input.GetKey(moveDown))
        {
            _verticalDirection = -1;
        }
        else
        {
            _verticalDirection = 0;
        }
        if (Input.GetKey(moveRight))
        {
            _horizontalDirection = 1;
        }
        else if (Input.GetKey(moveLeft))
        {
            _horizontalDirection = -1;
        }
        else
        {
            _horizontalDirection = 0;
        }
        if (Mathf.Abs(_horizontalDirection) == 1 || Mathf.Abs(_verticalDirection) == 1)
        {
            _rigidBody.velocity = new Vector2(_horizontalDirection * moveSpeed, _verticalDirection * moveSpeed);
        }
        else
        {
            _rigidBody.velocity = Vector2.zero;
        }

    }
}
